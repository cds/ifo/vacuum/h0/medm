#!/usr/bin/env python

# $Header$

# Patrick J. Thomas, California Institute of Technology, LIGO Hanford


import json
import re

from string import Template


def generate_medms (data):
	for i in data:
		target_file = i['target']
		template_file = i['template']
		substitutions = i['substitutions']

		template_file_path = './Templates/' + template_file
		f = open(template_file_path, 'r')
		template_text = f.read()
		f.close()

		# Replaces $(...) with ${...} in template_text.
		template_text_braces = re.sub(r'\$\((\S*)\)', r'${\1}', template_text)

		template = Template(template_text_braces)
		target_text = template.safe_substitute(substitutions)

		target_file_path = '../Target/' + target_file
		f = open(target_file_path, 'w')
		f.write(target_text)
		f.close()


f = open('H0VACLX.json', 'r')
data = json.load(f)
f.close()
generate_medms(data)

f = open('H0VACLY.json', 'r')
data = json.load(f)
f.close()
generate_medms(data)

f = open('H0VACMR.json', 'r')
data = json.load(f)
f.close()
generate_medms(data)

f = open('H0VACMX.json', 'r')
data = json.load(f)
f.close()
generate_medms(data)

f = open('H0VACMY.json', 'r')
data = json.load(f)
f.close()
generate_medms(data)

f = open('H0VACEX.json', 'r')
data = json.load(f)
f.close()
generate_medms(data)

f = open('H0VACEY.json', 'r')
data = json.load(f)
f.close()
generate_medms(data)

